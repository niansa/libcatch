#include <stdbool.h>

extern unsigned char libcatch_behavior;
extern bool libcatch_faulty;


void libcatch_init();
void throw(char *exprstr);


#define STRINGIZE_DETAIL(x) #x
#define STRINGIZE(x) STRINGIZE_DETAIL(x)
#define GETEXPRSTR(expr) "(line "STRINGIZE(__LINE__)") "#expr
#define ASSERT(expr, exprstr) if (expr) {throw(exprstr);}
#define CATCH1(expr) ASSERT((int)(expr) < 1, GETEXPRSTR(expr))
#define CATCH2(expr) ASSERT((int)(expr) != 0, GETEXPRSTR(expr))
