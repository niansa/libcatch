# libcatch
A C library to catch and traceback library function errors nicely

## Usage
 - Initialise libcatch as early as possible: `libcatch_init()`
 - Set behavior `libcatch_behavior = x` (see Behaviors)
 - `throw()` at any time
 - Use `CATCH(expr)` for library functions or `CATCHNZ(expr)` for system calls

## Behaviors
Not risking instability:
 - 0: Don't continue
 - 1: Continue once
 - 2: Always continue
 
Risking instability: (`libcatch_faulty` will be set on fault)
 - 3: Continue once
 - 4: Always continue
